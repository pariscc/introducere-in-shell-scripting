#!/bin/sh

ARG1=3
ARG2=2

printf "intruduceti prima cifra: "
read -r x
printf "intruduceti a doua cifra: "
read -r y

suma() {
  param1="$1"
  param2="$2"

  printf "suma este: %s\n" "$(($param1 + $param2))"
}

printf "afisare suma cu intrarile date de utilizator:\n"
suma "$x" "$y"

printf "afisare suma cu valori prestabilite:\n"
suma "$ARG1" "$ARG2"
