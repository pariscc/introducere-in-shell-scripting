#!/bin/sh

tipuri="
  sh
  bash
  zsh
  csh
"

printf "tipuri de shell-uri:\n"

for tip in $tipuri; do
  printf "  - $tip\n"
done
